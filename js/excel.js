function saveCSV(fileName){
    let downLink = document.querySelector(".js_down");
    let csv = ''; 
    let rows = Array.from(document.querySelectorAll(".js_table tr"));

    for(let i=0; i<rows.length; i++){
        let cells = Array.from(rows[i].querySelectorAll("td, th"));
        let row = [];
        cells.forEach((cell)=>{
            row.push(cell.innerText);
        });
        csv += row.join(',') + (i !== rows.length - 1 ? '\n':'' );
    };

    let csvFile = new Blob([csv], {type: "text/csv"});
    downLink.href = window.URL.createObjectURL(csvFile);
    downLink.download = fileName;
}

document.querySelector(".js_down").addEventListener('click', function(event){
    const { target } = event;
    let fileName = `${target.dataset.name}.csv`;
    saveCSV(fileName);
    return false;
});