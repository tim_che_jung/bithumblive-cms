// 글자 수 세기
const js_count_inputes = document.querySelectorAll(".js_count_input");

js_count_inputes.forEach((count)=>{
    count.addEventListener("input", (event)=>{
        const { target } = event;        
        const wrap = target.closest(".wrap_window_input");
        const count = wrap.querySelector(".js_count");
        count.innerText = target.value.length;
    });
});

// kakao adress
// 우편번호 찾기 찾기 화면을 넣을 element
const element_wrap = document.getElementById('daum_code');

function foldDaumPostcode() {
    element_wrap.style.display = 'none';
}

function execDaumPostcode() {
    // 현재 scroll 위치를 저장해놓는다.
    var currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
    new daum.Postcode({
        oncomplete: function(data) {
            // 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

            // 각 주소의 노출 규칙에 따라 주소를 조합한다.
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var addr = ''; // 주소 변수
            var extraAddr = ''; // 참고항목 변수

            //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                addr = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                addr = data.jibunAddress;
            }

            // 우편번호와 주소 정보를 해당 필드에 넣는다.
            document.getElementById('daum_postcode').value = data.zonecode;
            document.getElementById("daum_address").value = addr;
            // 커서를 상세주소 필드로 이동한다.
            document.getElementById("daum_detailAddress").focus();

            // iframe을 넣은 element를 안보이게 한다.
            // (autoClose:false 기능을 이용한다면, 아래 코드를 제거해야 화면에서 사라지지 않는다.)
            element_wrap.style.display = 'none';

            // 우편번호 찾기 화면이 보이기 이전으로 scroll 위치를 되돌린다.
            document.body.scrollTop = currentScroll;
        },
        // 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
        onresize : function(size) {
            element_wrap.style.height = size.height+'px';
        },
        width : '100%',
        height : '100%'
    }).embed(element_wrap);

    // iframe을 넣은 element를 보이게 한다.
    element_wrap.style.display = 'block';
}

const js_save = document.querySelector(".js_save");

// 새창 저장하기
const save_detail = () => {
    alert("저장");
    window.close();
}

let count_none = 0;
js_save.addEventListener("click", ()=>{
    const js_requires = Array.from(document.querySelectorAll(".js_require"));
    // 1. 필수 입력 카운트 있으면 2 없으면 else로 
    // 2. 미입력 카운트
    // 2-1 미입력이 수 > 0
        // 2-1-1 표시
    // 2-2 미입력 수 = 0
        // 저장 고~    
    if(js_requires.length > 0) {
        js_requires.forEach((require)=>{
            if (require.value === "") {
                count_none += 1;
                // require.classList.add("empty");
            }
        });
        if (count_none > 0) {
            js_requires.forEach((require)=>{
                if (require.value === "") {
                    require.classList.add("empty");
                }
            });
        } else if (count_none === 0){
            save_detail();
        }
    } else {
        save_detail();
    }
});

// 경고 문구 삭제
const js_requires = Array.from(document.querySelectorAll(".js_require"));

js_requires.forEach((require)=>{
    require.addEventListener("input", (event)=>{
        const { target } = event;
        if(target.value !== "" && target.classList.contains("empty")){
            target.classList.remove("empty")
        }
        count_none = 0;
    });
});

// 제한있는 숫자 입력 input
const js_input_numberes = Array.from(document.querySelectorAll(".js_input_number"));

js_input_numberes.forEach((number)=>{
    number.addEventListener("keyup",(event)=>{
        const { target } = event;
        const tv = Number(target.value)
        const tm = Number(target.max);
        if(tv > tm) {
            target.value = "";
            target.setAttribute("placeholder", `${target.max}까지 입니다.`)
        }
    });
});

const js_input_jpeges = document.querySelectorAll(".js_input_jpeg");

// img upload
js_input_jpeges.forEach((files)=>{
    files.addEventListener("change",(event)=>{
        const  { target } = event;
        const wrap = target.closest(".js_wrap_files");
        const image_wrap = wrap.querySelector(".js_preview_images");
        const default_img = image_wrap.querySelector(".js_default_img")
        if(window.FileReader) {
            // file image
            const max_size = Number(target.dataset.size);

            if(!target.files[0].type.match(/image\//)) return;
            if(target.files[0].type.match('image/*') && (target.files[0].size/1024/1024 <= max_size)){
                let reader = new FileReader();
                reader.readAsDataURL(target.files[0])
                reader.onload = function(event){
                    const prev_img = image_wrap.querySelector("img");
                    if(prev_img){
                        image_wrap.removeChild(prev_img);
                    }
                    const img = document.createElement("img");
                    img.setAttribute("src", event.target.result);
                    if(default_img) {
                        image_wrap.removeChild(default_img);
                    }
                    image_wrap.appendChild(img);
                }
            } else if(!target.files[0].type.match('image/*')) {
                console.log("no match type")
            } else if(target.files[0].size/1024/1024 > 1){
                console.log("size too big")
            }
        }
    });
});

// 숏컷 mp4 upload
const js_shortcut_video = document.querySelector(".js_shortcut_video");

if(js_shortcut_video){
    js_shortcut_video.addEventListener("change", (event)=>{
        const { target } = event;
        const js_shortcut_text = document.querySelector(".js_shortcut_text");
    
        if(window.FileReader) {
            const max_size = Number(target.dataset.size);
            if(target.files[0].size/1024/1024 <= max_size) {
                js_shortcut_text.value = target.files[0].name;
            } else {
                console.log("size too big");
            }
        }
    });
}


// 회원정보 상세 이용정지, 강제탈퇴
const js_stop = document.querySelector(".js_stop");
const js_out = document.querySelector(".js_out");

if(js_stop){
    js_stop.addEventListener("click", ()=>{
        alert("이용정지");
    });
}
if(js_out) {
    js_out.addEventListener("click", ()=>{
        alert("강제 탈퇴");
    });
}