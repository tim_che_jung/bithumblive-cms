// header
$(".js_title").mouseenter(function(){
    $(this).siblings().stop().animate({height:"100px"},300);
});
$(".js_item_navi").mouseleave(function(){
    $(this).children(".wrap_twodepth").stop().animate({height:"0px"},300);
});

const js_linkes = document.querySelectorAll(".js_link");
const js_btn_selectes = document.querySelectorAll(".js_btn_select");

// select 방송, 카테고리 등에 쓰이는 select
js_btn_selectes.forEach((select)=>{
    select.addEventListener("click", (event)=>{
        const { target } = event;
        if(target.classList.contains("active")) {
            target.classList.remove("active")
        } else {
            js_btn_selectes.forEach((ex)=>{
                if(ex.classList.contains("active")) {
                    ex.classList.remove("active");
                }
            });
            target.classList.add("active")
        }
    });
});

js_linkes.forEach((js_link)=>{
    js_link.addEventListener("click", ()=>{
        const text = js_link.innerText;
        const wrap = js_link.closest(".wrap_select");
        select = wrap.querySelector(".js_btn_select");
        select.innerText = text;
        if(select.classList.contains("active")) {
            select.classList.remove("active");
        }
    });
});

document.addEventListener('click',function(e){
    const { target } =e;
    if(target.classList.contains("js_btn_select")) {
        return false;
    } else {
        js_btn_selectes.forEach((select)=>{
            select.classList.remove("active");
        });
    }
});

// 새창
function openWin(){  
    window.open("./mgt_broad_detail.html", "방송관리상세", "width=1024, height=768, toolbar=no, menubar=yes, scrollbars=no, resizable=no" );  
}
function openMember(){  
    window.open("./view_member_detail.html", "회원정보조회", "width=1024, height=768, toolbar=no, menubar=yes, scrollbars=no, resizable=no" );  
}
function openshortcut(){  
    window.open("./shortcut_detail.html", "숏컷정보조회", "width=1024, height=768, toolbar=no, menubar=yes, scrollbars=no, resizable=no" );  
}

// 방송정보 팝업 상품리스트 array
const arr = [
//     {
//         "num":"123456789",
//         "name":"[삼성전자] 갤럭시 버즈 프로 퍼플",
//         "sale_price":"110,000원",
//         "origin_price":"140,000원",
//         "img":"../images/buzz_1.png"
//     }
]

// 방송상품 등록
// 방송정보 팝업에서 선택 후 상품 리스트가 붙는 곳
const js_append = document.querySelector(".js_append");

// 방송정보 팝업에서 선택 후 상품 리스트가 만들어 지는 function
const createList = (array) => {
    const item = document.createElement("li");
    const wrap_conts = document.createElement("div");
    const img_box = document.createElement("div");
    const text_box = document.createElement("div");
    const img = document.createElement("img");
    const name_span = document.createElement("p");
    const price_box = document.createElement("div");
    const sale_price = document.createElement("span");
    const origin_price = document.createElement("span");
    const del_btn = document.createElement("button");

    wrap_conts.className = "wrap_conts";
    img_box.className = "box_img";
    text_box.className = "box_text";
    name_span.className = "text_name";
    price_box.className = "box_price";
    sale_price.className = "text_sale";
    origin_price.className = "text_origin";
    del_btn.className = "btn_trash";

    item.id = `item_${array.num}`;
    img.setAttribute("src", array.img);
    name_span.innerText = array.name;
    sale_price.innerText = array.sale_price;
    origin_price.innerText = array.origin_price;
    del_btn.innerText = "trash";

    // 휴지통 모양의 리스트 삭제 버튼 click event
    del_btn.addEventListener("click", ()=>{
        js_append.removeChild(item);    
    });
    
    img_box.appendChild(img);
    wrap_conts.appendChild(img_box);
    
    text_box.appendChild(name_span);    
    price_box.appendChild(sale_price);
    price_box.appendChild(origin_price);
    text_box.appendChild(price_box);
    wrap_conts.appendChild(text_box);
    
    item.appendChild(wrap_conts);
    item.appendChild(del_btn);
    js_append.appendChild(item);
}

// 방송정보 팝업 : 방송 상품 전체 선택/해제
const js_chk_all = document.querySelector(".js_chk_all");

if(js_chk_all) {
    js_chk_all.addEventListener("input", ()=>{
        const chk_input_only = Array.from(document.querySelectorAll(".chk_input_only"));
        chk_input_only.forEach((chk)=>{
            js_chk_all.checked ? chk.checked = true : chk.checked = false ;
        });
    });
    
    // 방송정보 팝업 저장 버튼 
    const js_save = document.querySelector(".js_save");
    
    js_save.addEventListener("click", ()=>{
        const checked = document.querySelectorAll(".js_enroll:checked");
        
        if(checked.length > 0) {
            checked.forEach((chk)=>{
                const wrap = chk.closest(".tr_info");
                const num = wrap.querySelector(".js_num").innerText;
                const name = wrap.querySelector(".js_name").innerText;
                const sale_price = wrap.querySelector(".js_discount").innerText;
                const origin_price = wrap.querySelector(".js_origin").innerText;
                const img = wrap.dataset.img;
                
                arr.push({
                    "num":num,
                    "name":name,
                    "sale_price":sale_price,
                    "origin_price":origin_price,
                    "img":img
                });
            });
        }
        arr.map((a)=>(
            createList(a)
        ));
    });
}

// info icon, pop : 각 페이지 제목 옆에 인포 아이콘 클릭
const js_open_info = document.querySelector(".js_open_info");

js_open_info.addEventListener("click", ()=>{
    const js_info_pop = document.querySelector(".js_info_pop");
    js_info_pop.style.display = "block";
});

const js_close_info = document.querySelector(".js_close_info");

js_close_info.addEventListener("click",()=>{
    const js_info_pop = document.querySelector(".js_info_pop");
    js_info_pop.style.display = "none";
});